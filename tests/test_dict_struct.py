import ast
import unittest
from unittest import TestCase

import json

from typing import List, Dict, Tuple
from typing_tools import DictStruct, AssignmentSupportable

# region Class Declarations
class Point(DictStruct):
    x: int
    y: int

class PointWithDefaults(DictStruct):
    x: int = 0
    y: int = 0
    
    def print_me(self) -> str:
        _l = list()
        _l.append(f"PointWithDefaults is: {self}")
        _l.append(f"PointWithDefaults X is: {self.x} and {self['x']} and {self.get('x')}")
        _l.append(f"PointWithDefaults Y is: {self.y} and {self['y']} and {self.get('y')}")
        return '\n'.join(_l)
        

class Circle(DictStruct):
    r: int
    c: Point

class Arc(DictStruct):
    region: Circle
    vector: Point
    angle: int

class Figure(AssignmentSupportable):
    points: List[Point]

class NamedFigure(AssignmentSupportable):
    points_dict: Dict[str, Point]

class Monster(AssignmentSupportable):
    eyes: List[Circle] = None
    # torso: Figure
    # arms: List[NamedFigure]
    arc_of_danger: Arc = None
#endregion

# region Test functions:
def create_circle() -> Circle:
    _point_dict = { 'x': 5, 'y': 18 }
    _circle_dict = { 'c': _point_dict, 'r': 6 }
    o = Circle(_circle_dict)
    return o
def create_figure() -> Figure:
    f = Figure()
    f.points = [{ 'x': 5, 'y': 18 }, Point(x=2, y=15), dict(x=1, y=22)]
    return f

class TestDictStruct(TestCase):
    def assertCircle(self, o: Circle, r:int, x:int, y:int):
        self.assertIsInstance(o, dict, "Circle is not an instance of a dict, but should be.")
        self.assertEqual(o.r, r, "Circle Radius failed equality compare.")
        self.assertEqual(o.c.x, x, "Circle Center X failed equality compare.")
        self.assertEqual(o.c.y, y, "Circle Center Y failed equality compare.")
        self.assertEqual(o, {'r': r, 'c': {'x': x, 'y': y}}, "Circle failed dict equality compare.")
        self.assertEqual(json.dumps(o, sort_keys=True), f"{{\"c\": {{\"x\": {x}, \"y\": {y}}}, \"r\": {r}}}", "Circle failed JSON equality compare.")

    def assertPointPrint(self, text: str, x:int = 0, y:int = 0):
        _lines = text.split('\n')
        _prefix = "PointWithDefaults is: "
        _empty, _sep, _dict = _lines[0].partition(_prefix)
        self.assertEqual(_empty, '')
        self.assertEqual(_sep, _prefix)
        _d = ast.literal_eval(_dict)
        self.assertEqual(_d, dict(x=x, y=y))
        self.assertEqual(_lines[1], f"PointWithDefaults X is: {x} and {x} and {x}")
        self.assertEqual(_lines[2], f"PointWithDefaults Y is: {y} and {y} and {y}")

    def test_circle_1(self):
        o = Circle(r=4, c=Point(x=2, y=15))
        self.assertCircle(o, 4, 2, 15)
                
    def test_circle_2(self):
        _point_dict = { 'x': 5, 'y': 18 }
        _circle_dict = { 'c': _point_dict, 'r': 6 }
        o = Circle(_circle_dict)
        self.assertCircle(o, 6, 5, 18)

    def test_default_values_point(self):
        p = PointWithDefaults(y=6)
        _text = p.print_me()
        self.assertPointPrint(_text, y=6)
        
        p.x = 8
        p.y = 4
        _text = p.print_me()
        self.assertPointPrint(_text, 8, 4)
    
    def test_mutable_circle(self):
        o = create_circle()
        self.assertNotEqual(o.r, 9)
        self.assertNotEqual(o['r'], 9)
        self.assertNotEqual(o.get('r'), 9)
        o.r = 9
        self.assertEqual(o.r, 9)
        self.assertEqual(o['r'], 9)
        self.assertEqual(o.get('r'), 9)

class TestAssignmentSupportable(TestCase):
    def test_figure(self):
        f = Figure()
        f.points = [{ 'x': 5, 'y': 18 }, Point(x=2, y=15), dict(x=1, y=22)]
        _expected_points = [ dict(x=5, y=18), dict(x=2, y=15), dict(x=1, y=22) ]
        self.assertNotIsInstance(f, dict, "Figure is an instance of a dict, but should not.")
        self.assertIsInstance(f.points, list, "Figure.points is not an instance of a list, but should be.")
        for i, _p in enumerate(f.points):
            self.assertEqual(_p, _expected_points[i])
            self.assertEqual(_p.x, _expected_points[i]['x'])
            self.assertEqual(_p.y, _expected_points[i]['y'])
        self.assertEqual(f.points, _expected_points)

    def test_named_figure(self):
        f2 = NamedFigure()
        f2.points_dict = { 'A': { 'x': 5, 'y': 18 }, 'PointB': Point(x=2, y=15), 'p.C': dict(x=1, y=22) }
        _expected_points = { 'A': dict(x=5, y=18), 'PointB': dict(x=2, y=15), 'p.C': dict(x=1, y=22) }
        self.assertNotIsInstance(f2, dict, "NamedFigure is an instance of a dict, but should not.")
        self.assertIsInstance(f2.points_dict, dict, "NamedFigure.points_dict is not an instance of a dict, but should be.")
        for _name, _p in f2.points_dict.items():
            self.assertEqual(_p, _expected_points[_name])
            self.assertEqual(_p.x, _expected_points[_name]['x'])
            self.assertEqual(_p.y, _expected_points[_name]['y'])
        self.assertEqual(f2.points_dict, _expected_points)

    def test_mutable_figure(self):
        f = create_figure()
        
        self.assertNotEqual(f.points, [ dict(x=1, y=1), dict(x=1, y=2), dict(x=2, y=3) ])
        f.points = [ Point(x=1, y=1), Point(x=1, y=2), Point(x=2, y=3) ]
        self.assertEqual(f.points, [ dict(x=1, y=1), dict(x=1, y=2), dict(x=2, y=3) ])
        
        self.assertNotEqual(f.points[0], dict(x=8, y=4))
        f.points[0] = Point(x=8, y=4)
        self.assertEqual(f.points[0], dict(x=8, y=4))
        
        self.assertNotEqual(f.points[1].x, 88)
        f.points[1].x = 88
        self.assertEqual(f.points[1].x, 88)
        
        self.assertEqual(f.points, [ dict(x=8, y=4), dict(x=88, y=2), dict(x=2, y=3) ])
    
    def test_mutable_monster(self):
        _point_dict = { 'x': 5, 'y': 18 }
        _circle_dict = { 'c': _point_dict, 'r': 6 }
        
        m = Monster()
        # m.arms = [ f2, NamedFigure(points_dict={'hand': Point(x=18, y=44), 'shoulder': {'x':10, 'y': 80}) ]
        if (not m.eyes):
            o = create_circle()
            m.eyes = [ o, Circle(_circle_dict), dict(r=5, c=dict(x=5,y=108)), {'r': 9, 'c': {'x':1, 'y':2}} ]
        if (not m.arc_of_danger):
            m.arc_of_danger = json.loads('{"region": { "c": {"x": -5, "y": -9}, "r": 50}, "vector": { "x": 500, "y": 866 }, "angle": 120 }') 
        self.assertNotIsInstance(m, dict, "Monster is an instance of a dict, but should not.")
        self.assertEqual(len(m.eyes), 4)
        self.assertEqual(m.arc_of_danger['region'].r, 50)
        
        print("Let's mutate than monster!")
        m.arc_of_danger.region.r += 15
        self.assertEqual(m.arc_of_danger.get('region').r, 65)
        m.eyes.append(dict(r=-5, c=dict(x=1, y=22)))
        self.assertEqual(len(m.eyes), 5)
        self.assertEqual(m.eyes[-1].c.y, 22)
#endregion

if (__name__ == '__main__'):
    unittest.main()
